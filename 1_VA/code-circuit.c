const byte LED1 = 13;
const byte LED2 = 12;
const byte LEDRESP = 7;

bool condNao(int);
bool condE(int, int);
bool condOu(int, int);
bool condNaoE(int, int);
bool condNaoOu(int, int);
bool condOuExclusivo(int, int);
bool condNaoOuExclusivo(int, int);


void setup()
{
	pinMode(LED1, OUTPUT);
    pinMode(LED2, OUTPUT);
	pinMode(LEDRESP, OUTPUT);
}


void loop()
{
  int entradas[4][2] = {{0, 0}, {1, 0}, {0, 1}, {1, 1}};
  portaLogicaE(entradas);
  portaLogicaOu(entradas);
  portaLogicaNaoE(entradas);
  portaLogicaNaoOu(entradas);
  portaLogicaOuExclusico(entradas);
  portaLogicaNaoOuExclusico(entradas);
}

void portaLogicaE(int entradas[][2]) {
  int i;
  
  for (i = 0; i < 4; i++) {
    int led1 = entradas[i][0];
    int led2 = entradas[i][1];
    int ledResp = condE(led1, led2);

    digitalWrite(LED1, led1);
    digitalWrite(LED2, led2);
    digitalWrite(LEDRESP, ledResp);
    delay(2000);
  }
}

void portaLogicaOu(int entradas[][2]) {
  int i;
  
  for (i = 0; i < 4; i++) {
    int led1 = entradas[i][0];
    int led2 = entradas[i][1];
    int ledResp = condOu(led1, led2);

    digitalWrite(LED1, led1);
    digitalWrite(LED2, led2);
    digitalWrite(LEDRESP, ledResp);
    delay(2000);
  }
}

void portaLogicaNaoE(int entradas[][2]) {
  int i;
  
  for (i = 0; i < 4; i++) {
    int led1 = entradas[i][0];
    int led2 = entradas[i][1];
    int ledResp = condNaoE(led1, led2);

    digitalWrite(LED1, led1);
    digitalWrite(LED2, led2);
    digitalWrite(LEDRESP, ledResp);
    delay(2000);
  }
}

void portaLogicaNaoOu(int entradas[][2]) {
  int i;
  
  for (i = 0; i < 4; i++) {
    int led1 = entradas[i][0];
    int led2 = entradas[i][1];
    int ledResp = condNaoOu(led1, led2);

    digitalWrite(LED1, led1);
    digitalWrite(LED2, led2);
    digitalWrite(LEDRESP, ledResp);
    delay(2000);
  }
}

void portaLogicaOuExclusico(int entradas[][2]) {
  int i;
  
  for (i = 0; i < 4; i++) {
    int led1 = entradas[i][0];
    int led2 = entradas[i][1];
    int ledResp = condOuExclusivo(led1, led2);

    digitalWrite(LED1, led1);
    digitalWrite(LED2, led2);
    digitalWrite(LEDRESP, ledResp);
    delay(2000);
  }
}


void portaLogicaNaoOuExclusico(int entradas[][2]) {
  int i;
  
  for (i = 0; i < 4; i++) {
    int led1 = entradas[i][0];
    int led2 = entradas[i][1];
    int ledResp = condNaoOuExclusivo(led1, led2);

    digitalWrite(LED1, led1);
    digitalWrite(LED2, led2);
    digitalWrite(LEDRESP, ledResp);
    delay(2000);
  }
}

bool condNao(int entrada1) {
	return entrada1 == 0;
}

bool condE(int entrada1, int entrada2) {
	return (entrada1 * entrada2) == 1;
}
return int

bool condOu(int entrada1, int entrada2) {
	return (entrada1 + entrada2) > 0;
}


bool condNaoE(int entrada1, int entrada2) {
	return condNao((entrada1 * entrada2) == 1);
}

bool condNaoOu(int entrada1, int entrada2) {
	return condNao((entrada1 + entrada2) > 0);
}

bool condOuExclusivo(int entrada1, int entrada2) {
	return entrada1 != entrada2;
}

bool condNaoOuExclusivo(int entrada1, int entrada2) {
	return condNao(entrada1 != entrada2);
}
